/*!
  \file
*/
#include <Arduino.h>///< Виконання функцій Arduino
#include <Wire.h>///< Робота з I2C
#include <RTClibExtended.h>///<  Робота з модулем годинника
#include <LiquidCrystal_I2C.h>///< Робота з дисплеєм
#include <Preferences.h>///< Зберігання даних до зовнішньої пам'яті

#define BUTTON_PIN 15///< Пін для зчитування кнопок керування
#define SHED_PIN 17///< Пін для зчитування перемикача типів розкладів
#define REL_PIN 18///< Пін для керування реле
#define ALARM_PIN 16///< Пін для зчитування кнопки алярми

LiquidCrystal_I2C lcd(0x3F,16,2);///< Ініціалізація керування дисплеєм та його підключення

const byte DS3231 = 0x68;///< Підключення годинникового модуля
RTC_DS3231 rtc;///< Ініціалізація керування годинником

int current_menu;///< Змінна, яка визначає номер поточного меню

unsigned long timing;///< Змінна для використання часових методів

//Змінні для антидребезгу кнопок
int button = 0;
int buttonState;

int lastButtonState = LOW;
long lastDebounceTime = 0; 
long debounceDelay = 50; 
////////////

//bool din;///////////////////
bool last = false;///< Змінна для запобігання повторних дзвінків в непотрібний час

/// Клас Розклад
class Shedule{
  public:
    int shedNum;///< Змінна, що визначає поточний тип розкладу (1 - Звичайний, 2 - Короткий)
    int startHour = 7;///< Година першого дзвінку
    int startMinute = 0;///< Хвилина першого дзвінку

    int NringNum = 12;///< Максимальна кількість дзвінків на день для звичайного розкладу 
    int Nring[12];///< Масив, у якому зберігаються хвилини до наступного дзвінка в звичайному розкладі

    int SringNum = 12;///< Максимальна кількість дзвінків на день для короткого розкладу 
    int Sring[12];///< Масив, у якому зберігаються хвилини до наступного дзвінка в короткому розкладі

    String shedType;///< Текстова змінна, що відповідає змінній shedNum
    void ShedText(){
      switch (shedNum){
        case 1:
          shedType = "Normal";
          break;
        case 2:
          shedType = "Short";
          break;
      }
    }
    bool set;///< Змінна для налаштування часу дзвінків

    int nextH;///< Година наступного спрацьовування дзвінка 
    int nextM;///< Хвилина наступного спрацьовування дзвінка
    int nextNum;///< Порядковий номер наступного дзвінка
    int maxN;///< Останній заповнений номер дзвінка (Звичайний розклад)
    int maxS;///< Останній заповнений номер дзвінка (Короткий розклад)
};

/// Клас Налаштування
class Setting {
  public:
    int setpar;///< Порядковий номер параметру для налаштування
    int ringdur;///< Тривалість звичайного дзвінка
    int setal;///< Порядковий номер для налаштування параметрів алярми
    int aldur,///< Загальна тривалість алярми 
        alnum,///< Кількість повторних дзвінків алярми
        alring,///< Тривалість дзвінка алярми
        alpause,///< Тривалість паузи між дзвінками алярми
        alwait;///< Тривалість паузи між циклами алярми
    bool sets;///< Налаштування конкретного параметру
    int date;///< Порядковий номер даних для налаштування дати і часу (1 - Рік, 2 - Місяць, 3 - День, 4 - Година, 5 - Хвилина)
    bool dset;///< Налаштування конкретного параметру дати і часу
    bool reset;///< Визначення чи скинуті налаштування
};

/// Клас Дисплей
class LCD_Out {
  public:
    String UpText;///< Текстова змінна для виводу на перший рядок дисплею
    String DownText;///< Текстова змінна для виводу на другий рядок дисплею
};

//Ініціалізація класів
Shedule shed;///< Розклад
Setting set;///< Налаштування
LCD_Out out;///< Дисплей
Preferences prefs;///< Збереження даних

/*!
  \brief Допомагає обраховувати час наступного дзвінка при запуску пристрою, використовується для усунення помилок під час перезапуску пристрою.
*/
void Before(){
  /*!
    Використовується як доповнення до другого порівняння у функції NextRing з різницею у тому, що відмотує час назад.
    Також виконує обчислення максимального номеру дзвінка.
  */
  shed.maxN =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Nring[i] != 0){
      shed.maxN +=1;
    }
  }
  shed.maxS =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Sring[i] != 0){
      shed.maxS +=1;
    }
  }
  
      if(shed.nextNum != 0 && shed.nextNum <= shed.maxN){
        last = false;
      }

  if(shed.shedNum == 1){
    if(shed.nextNum > shed.maxN){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
    }
  } else if(shed.shedNum == 2){
    if(shed.nextNum > shed.maxS){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
    }
  }
  DateTime now = rtc.now();
  if(now.hour() < shed.nextH || (now.hour() <= shed.nextH && now.minute() < shed.nextM)){
          shed.nextH = shed.startHour;
          shed.nextM = shed.startMinute;
          if (shed.shedNum == 1){
            for (;now.hour() < shed.nextH || (now.hour() <= shed.nextH && now.minute() < shed.nextM);){
              for (int i = 0; i < shed.Nring[shed.nextNum];i++){
                  if(shed.nextM != 0){
                    shed.nextM -= 1;
                  } else {
                    if(shed.nextH != 0){
                      shed.nextM = 59;
                      shed.nextH-=1;
                    } else {
                      shed.nextM = 59;
                      shed.nextH = 23;
                    }
                  }
              }
              if (now.hour() < shed.nextH || (now.hour() <= shed.nextH && now.minute() < shed.nextM)){
                shed.nextNum-=1;
              }
            }
          } else if (shed.shedNum == 2){
            for (;now.hour() < shed.nextH || (now.hour() <= shed.nextH && now.minute() < shed.nextM);){
              for (int i = 0; i < shed.Sring[shed.nextNum];i++){
                  if(shed.nextM != 0){
                    shed.nextM -= 1;
                  } else {
                    if(shed.nextH != 0){
                      shed.nextM = 59;
                      shed.nextH-=1;
                    } else {
                      shed.nextM = 59;
                      shed.nextH = 23;
                    }
                  }
              }
              if (now.hour() < shed.nextH || (now.hour() <= shed.nextH && now.minute() < shed.nextM)){
                shed.nextNum-=1;
              }
            }
          }
        }
}
/*!
  \brief "Стартова точка"
*/
void setup() {
  Serial.begin(115200);
  /// Ініціалізація всіх процесів:
  /*!
    1) Ініціалізація підключення до дисплею
    \code
      lcd.begin(16,2);
      lcd.init();                     
      lcd.backlight();
    \endcode
  */
  lcd.begin(16,2);
  current_menu =1;
  delay(100);
  lcd.init();                     
  lcd.backlight();
  /*!
    2) Перевірка годинника на помилки
    \code
    if (! rtc.begin()) {
      Serial.println("Couldn't find RTC");
      out.UpText = "Error!";
      out.DownText = "Timer isnt alright";
      while (1);
    }
    \endcode
  */
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    out.UpText = "Error!";
    out.DownText = "Timer isnt alright";
    while (1);
  }
  //rtc.adjust(DateTime(DateTime(F(__DATE__), F(__TIME__))));
  /*!
    3) Ініціалізація Preferences і завантаження даних з постійної пам'яті
    \code
    prefs.begin("Settings",false);
    //Завантаження налаштувань алярми
    if(prefs.getInt("RingDur") == 0){
      set.ringdur = 5;
    } else {
      set.ringdur = prefs.getInt("RingDur");
    }
    if(prefs.getInt("AlDur")==0){
      set.aldur = 30;
    } else {
      set.aldur = prefs.getInt("AlDur");
    }
    if(prefs.getInt("AlNum")==0){
      set.alnum = 3;
    } else {
      set.alnum = prefs.getInt("AlNum");
    }
    if(prefs.getInt("AlRing")==0){
      set.alring = 5;
    } else {
      set.alring = prefs.getInt("AlRing");
    }
    if(prefs.getInt("AlPaus1")==0){
      set.alpause = 1;
    } else {
      set.alpause = prefs.getInt("AlPaus1");
    }
    if(prefs.getInt("AlPaus2")==0){
      set.alwait = 10;
    } else {
      set.alwait = prefs.getInt("AlPaus2");
    }
    shed.startHour = prefs.getInt("startHour");// Година початкового дзвінка
    shed.startMinute = prefs.getInt("startMinute");// Хвилина початкового дзвінка
    //Змінна, що зберігає кількість хвилин до наступного дзвінка (Звичайний розклад)  
    shed.Nring[1] = prefs.getInt("rn1");
    shed.Nring[2] = prefs.getInt("rn2");
    shed.Nring[3] = prefs.getInt("rn3");
    shed.Nring[4] = prefs.getInt("rn4");
    shed.Nring[5] = prefs.getInt("rn5");
    shed.Nring[6] = prefs.getInt("rn6");
    shed.Nring[7] = prefs.getInt("rn7");
    shed.Nring[8] = prefs.getInt("rn8");
    shed.Nring[9] = prefs.getInt("rn9");
    shed.Nring[10] = prefs.getInt("rn10");
    shed.Nring[11] = prefs.getInt("rn11");
    //Змінна, що зберігає кількість хвилин до наступного дзвінка (Короткий розклад)  
    shed.Sring[1] = prefs.getInt("rs1");
    shed.Sring[2] = prefs.getInt("rs2");
    shed.Sring[3] = prefs.getInt("rs3");
    shed.Sring[4] = prefs.getInt("rs4");
    shed.Sring[5] = prefs.getInt("rs5");
    shed.Sring[6] = prefs.getInt("rs6");
    shed.Sring[7] = prefs.getInt("rs7");
    shed.Sring[8] = prefs.getInt("rs8");
    shed.Sring[9] = prefs.getInt("rs9");
    shed.Sring[10] = prefs.getInt("rs10");
    shed.Sring[11] = prefs.getInt("rs11");
    \endcode
  */
  prefs.begin("Settings",false);
  set.setpar = 1;
  if(prefs.getInt("RingDur") == 0){
    set.ringdur = 5;
  } else {
    set.ringdur = prefs.getInt("RingDur");
  }
  if(prefs.getInt("AlDur")==0){
    set.aldur = 30;
  } else {
    set.aldur = prefs.getInt("AlDur");
  }
  if(prefs.getInt("AlNum")==0){
    set.alnum = 3;
  } else {
    set.alnum = prefs.getInt("AlNum");
  }
  if(prefs.getInt("AlRing")==0){
    set.alring = 5;
  } else {
    set.alring = prefs.getInt("AlRing");
  }
  if(prefs.getInt("AlPaus1")==0){
    set.alpause = 1;
  } else {
    set.alpause = prefs.getInt("AlPaus1");
  }
  if(prefs.getInt("AlPaus2")==0){
    set.alwait = 10;
  } else {
    set.alwait = prefs.getInt("AlPaus2");
  }
  shed.startHour = prefs.getInt("startHour");
  shed.startMinute = prefs.getInt("startMinute");
  shed.Nring[1] = prefs.getInt("rn1");
  shed.Nring[2] = prefs.getInt("rn2");
  shed.Nring[3] = prefs.getInt("rn3");
  shed.Nring[4] = prefs.getInt("rn4");
  shed.Nring[5] = prefs.getInt("rn5");
  shed.Nring[6] = prefs.getInt("rn6");
  shed.Nring[7] = prefs.getInt("rn7");
  shed.Nring[8] = prefs.getInt("rn8");
  shed.Nring[9] = prefs.getInt("rn9");
  shed.Nring[10] = prefs.getInt("rn10");
  shed.Nring[11] = prefs.getInt("rn11");

  shed.Sring[1] = prefs.getInt("rs1");
  shed.Sring[2] = prefs.getInt("rs2");
  shed.Sring[3] = prefs.getInt("rs3");
  shed.Sring[4] = prefs.getInt("rs4");
  shed.Sring[5] = prefs.getInt("rs5");
  shed.Sring[6] = prefs.getInt("rs6");
  shed.Sring[7] = prefs.getInt("rs7");
  shed.Sring[8] = prefs.getInt("rs8");
  shed.Sring[9] = prefs.getInt("rs9");
  shed.Sring[10] = prefs.getInt("rs10");
  shed.Sring[11] = prefs.getInt("rs11");
 
  //prefs.end();
  /*!
    4) Скидання деяких змінних
    \code
    shed.set = false;
    set.sets = false;
    set.date = 1;
    set.dset = false;
    set.setal = 1;
    shed.NringNum = 0;
    shed.SringNum = 0;
    shed.nextNum = 0;
    \endcode
  */
  shed.set = false;
  set.sets = false;
  set.date = 1;
  set.dset = false;
  set.setal = 1;
  shed.NringNum = 0;
  shed.SringNum = 0;
  shed.nextNum = 0;

  /*!
    5) Ініціалізація роботи з пінами і вимкнення реле
    \code
    pinMode(BUTTON_PIN,INPUT);
    pinMode(SHED_PIN, INPUT_PULLUP);
    pinMode(REL_PIN, OUTPUT);
    pinMode(ALARM_PIN, INPUT_PULLUP);
    digitalWrite(REL_PIN, HIGH);
    \endcode
  */
  pinMode(BUTTON_PIN,INPUT);
  pinMode(SHED_PIN, INPUT_PULLUP);
  pinMode(REL_PIN, OUTPUT);
  pinMode(ALARM_PIN, INPUT_PULLUP);
  digitalWrite(REL_PIN, HIGH);

  /*!
    6) Визначення поточного типу розкладу за станом перемикача
    \code
    bool shedule = digitalRead(SHED_PIN);
    if(shedule == 0){
      shed.shedNum = 1;
    }
    else {
      shed.shedNum = 2;
    }
    \endcode
  */
  bool shedule = digitalRead(SHED_PIN);
  if(shedule == 0){
    shed.shedNum = 1;
  }
  else {
    shed.shedNum = 2;
  }

  /*!
    7) Визначення номеру останнього дзвінка (для обидвох типів розкладів)
    \code
    shed.maxN =0;
    for(int i = 1; i <= 11;i++){
      if(shed.Nring[i] != 0){
        shed.maxN +=1;
      }
    }
    shed.maxS =0;
    for(int i = 1; i <= 11;i++){
      if(shed.Sring[i] != 0){
        shed.maxS +=1;
      }
    }
    \endcode
  */
  shed.maxN =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Nring[i] != 0){
      shed.maxN +=1;
    }
  }
  shed.maxS =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Sring[i] != 0){
      shed.maxS +=1;
    }
  }

  /*!
    8) Виклик функції Before
  */
  Before();
  Serial.println(digitalRead(SHED_PIN));
}
/*!
  \brief Зчитує дані з BUTTON_PIN та конвертує їх у числа-порядкові номери кнопок.
*/
int buttonRead(){
  /*!
    Зчитування даних з піну і їх запис до змінної. 
    \code
    int reading = analogRead(BUTTON_PIN);
    \endcode
    Присвоєння значення змінній out в залежності від reading і її повернення.
    \code
    int out = 0;
    if (reading != 4095) {
          if (reading < 1000) {
            out = 4;
          } else if (reading < 1400) {
            out = 3;
          } else if (reading < 2000) {
            out = 2;
          } else if (reading < 4100) {
            out = 1;
          }
    } else {
      out = 0;
    }
    return out;
    \endcode
  */
  int reading = analogRead(BUTTON_PIN);
  int out = 0;
  if (reading != 4095) {
        if (reading < 1000) {
          out = 4;
        } else if (reading < 1400) {
          out = 3;
        } else if (reading < 2000) {
          out = 2;
        } else if (reading < 4100) {
          out = 1;
        }
  } else {
    out = 0;
  }

  return out;
}
/*!
  \brief Виконує антидребезг даних, що отримує buttonRead.
*/
int readButton(){
  /*!
    Зчитування показників кнопки лише в момент натиску на неї.
    В подальшому буде використовуватися разультат саме цієї змінної.
  */
  if (buttonRead() != lastButtonState) {
    lastDebounceTime = millis();
  } 

  button = 0;

  if ((millis() - lastDebounceTime) > debounceDelay) {
    if (buttonRead() != buttonState) {
      buttonState = buttonRead();
      if (buttonState != 0){
        button = buttonState; 
      }
    }
  }
  
  //Serial.println(button);

  lastButtonState = buttonRead();
  //delay(10);
  //Serial.println(button);
  return button;
}
/*!
  \brief Обчислення високосності року, впливає лише на кількість днів у місяці Лютому.
*/
int fullYear(){
  /*!
    Функція зчитує показники з годинника і обчислює значення змінної full в залежності від відповідності умовам високосного року.
    \code
    DateTime now = rtc.now();
    bool full;
    for(int i = 2000; i <= now.year(); i+=4){
      if(i == now.year()){
        full = true;
      } else {
        full = false;
      }
    }
    return full;
    \endcode
  */
  DateTime now = rtc.now();
  bool full;
  for(int i = 2000; i <= now.year(); i+=4){
    if(i == now.year()){
      full = true;
    } else {
      full = false;
    }
  }
  return full;
}
/*!
  \brief Обчислює кількість днів у кожному місяці.
*/
int dayInMonth(){
  /*!
    Зчитує данні з годинника, а саме номер місяця, і в залежності від отриманих даних надає змінній days необхідного значення.
    \code
    DateTime now = rtc.now();
  int days = 0;
  switch (now.month())
  {
  case 1:
    days = 31;
    break;
  case 2:
    if(fullYear()==false){
      days = 28;
    } else {
      days = 29;
    }
    break;
  case 3:
    days = 31;
    break;
  case 4:
    days = 30;
    break;
  case 5:
    days = 31;
    break;
  case 6:
    days = 30;
    break;
  case 7:
    days = 31;
    break;
  case 8:
    days = 31;
    break;
  case 9:
    days = 30;
    break;
  case 10:
    days = 31;
    break;
  case 11:
    days = 30;
    break;
  case 12:
    days = 31;
    break;
  }
  return days;
    \endcode
  */
  DateTime now = rtc.now();
  int days = 0;
  switch (now.month())
  {
  case 1:
    days = 31;
    break;
  case 2:
    if(fullYear()==false){
      days = 28;
    } else {
      days = 29;
    }
    break;
  case 3:
    days = 31;
    break;
  case 4:
    days = 30;
    break;
  case 5:
    days = 31;
    break;
  case 6:
    days = 30;
    break;
  case 7:
    days = 31;
    break;
  case 8:
    days = 31;
    break;
  case 9:
    days = 30;
    break;
  case 10:
    days = 31;
    break;
  case 11:
    days = 30;
    break;
  case 12:
    days = 31;
    break;
  }
  return days;
}
/*!
  \brief Головне меню.
*/
void mainMenu(){
  /*!
    Зчитування даних з годинника і їх форматування
    \code
    DateTime now = rtc.now();
    String year = String(now.year());
    String month = String(now.month());
    String day = String(now.day());

    String hour = String(now.hour());
    String minute = String(now.minute());
    if(month.length() < 2){
        month = "0" + String(now.month());
      }
      else {
        month = String(now.month());
      }
      if(day.length() < 2){
        day = "0" + String(now.day());
      }
      else {
        day = String(now.day());
      }
      if(hour.length() < 2){
        hour = "0" + String(now.hour());
      }
      else {
        hour = String(now.hour());
      }
      if(minute.length() < 2){
        minute = "0" + String(now.minute());
      }
      else{
        minute = String(now.minute());
      }
    \endcode
    Виведення усіх даних дати і часу на дисплей за допомогою змінної out.UpText
    \code
    out.UpText = year + "." + month + "." + day + " " + hour + ":" + minute;
    \endcode
  */
  int lastShed = shed.shedNum;
  //char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
  DateTime now = rtc.now();
  String year = String(now.year());
  String month = String(now.month());
  String day = String(now.day());

  String hour = String(now.hour());
  String minute = String(now.minute());
  if(month.length() < 2){
      month = "0" + String(now.month());
    }
    else {
      month = String(now.month());
    }
    if(day.length() < 2){
      day = "0" + String(now.day());
    }
    else {
      day = String(now.day());
    }
    if(hour.length() < 2){
      hour = "0" + String(now.hour());
    }
    else {
      hour = String(now.hour());
    }
    if(minute.length() < 2){
      minute = "0" + String(now.minute());
    }
    else{
      minute = String(now.minute());
    }
  out.UpText = year + "." + month + "." + day + " " + hour + ":" + minute;
  shed.ShedText();
  //out.DownText = "Shedule: " + String(shed.shedType);
  String nH = String(shed.nextH);
  String nM = String(shed.nextM);
  if(nH.length() < 2){
        nH = "0" + String(shed.nextH);
      } else {
        nH = String(shed.nextH);
      }
      if(nM.length() < 2){
        nM = "0" + String(shed.nextM);
      } else {
        nM = String(shed.nextM);
      }
  out.DownText = nH + ":" + nM + " " + String(shed.shedType);
  int button = readButton();
  bool shedule = digitalRead(SHED_PIN);
  if(shedule == 0){
    shed.shedNum = 1;
  }
  else {
    shed.shedNum = 2;
  }
  switch (button)
  {
  case 1:
    current_menu = 4;
    break;
  case 4:
    current_menu = 2;
    break;
  default:
    break;
  }
  if(lastShed != shed.shedNum){
    Before();
  }
}
/*!
  \brief Точка входу до меню налаштувань.
*/
void settingsMenu(){
  /*!
    Вивід тексту на дисплей
    \code
    out.UpText = "Settings";
    out.DownText = " ";
    \endcode
    Зчитуваня даних з пінів кнопок і розкладу
    \code
    out.UpText = "Settings";
    out.DownText = " ";
    int button = readButton();
    \endcode
    Надання змінній shed.shedNum значення в залежності від показників піна розкладу
    \code
    bool shedule = digitalRead(SHED_PIN);
    if(shedule == 0){
      shed.shedNum = 1;
    }
    else {
      shed.shedNum = 2;
    }
    \endcode
    Перехід до іншого меню в залежності від  натиснутої кнокпи (readbutton)
    \code
    switch (button)
    {
    case 1:
      current_menu = 1;
      break;
    case 2:
      current_menu = 3;
      break;
    case 4:
      current_menu = 4;
      break;
    default:
      break;
    }
    \endcode
  */
  int lastShed = shed.shedNum;
  out.UpText = "Settings";
  out.DownText = " ";
  int button = readButton();
  bool shedule = digitalRead(SHED_PIN);
  if(shedule == 0){
    shed.shedNum = 1;
  }
  else {
    shed.shedNum = 2;
  }
  switch (button)
  {
  case 1:
    current_menu = 1;
    break;
  case 2:
    current_menu = 3;
    break;
  case 4:
    current_menu = 4;
    break;
  default:
    break;
  }
  if(lastShed != shed.shedNum){
    Before();
  }
}
/*!
  \brief Меню безпосередньо налаштувань.
*/
void Settings(){
  /*!
    Зчитування даних із годинника синхронізація локальних змін з класовими
    \code
    DateTime now = rtc.now();
    int setpar = set.setpar;
    int button = readButton();
    int ringdur = set.ringdur;
    bool sets = set.sets;
    int date = set.date;
    bool dset = set.dset;
    \endcode
    Форматування тексту дати та часу
    \code
    String year = String(now.year());
    String month = String(now.month());
    String day = String(now.day());
    String hour = String(now.hour());
    String minute = String(now.minute());
    if(month.length() < 2){
      month = "0" + String(now.month());
    }
    else {
      month = String(now.month());
    }
    if(day.length() < 2){
      day = "0" + String(now.day());
    }
    else {
      String day = String(now.day());
    }
    if(hour.length() < 2){
      hour = "0" + String(now.hour());
    }
    else {
      String hour = String(now.hour());
    }
    if(minute.length() < 2){
      minute = "0" + String(now.minute());
    }
    else{
      String minute = String(now.minute());
    }
    \endcode
    Зчитування змінної sets. Якщо змінна дорівнює false:
      Вивід на дисплей назви параметру та його поточного значення.
      Зчитування кнопок "<" та ">" для можливості вибору між усіма параметрами.
      Зчитування кнопки "X" для можливості надати змінній sets значення true.
      Зчитування кнопки "О" для можливості повернутися до виконання функції settingsMenu.
    Зчитування змінної setpar з класу Setting, наступні дії залежать від значення цієї змінної:
     1)
        Зчитування кнопок "<" та ">" для можливості вибору значення змінної ringdur класу Setting, що відповідає за параметр Тривалість дзвінка.
        Зчитування кнопки "О" для можливості підтвердження змін та зміни значення sets на false.
     2)
        Зчитування кнопки "Х" для можливості зміни значення current_menu на 6 і переходу до функції Alarm.
     3)
        Зчитування кнопок "<" та ">" для можливості вибору значення змінної date класу Setting, що відповідає за вибір параметру дати (рік, місяць, день, година, хвилина).
        Зчитування кнопки "Х" для можливості зміни значення змінної dset класу Setting на true (див. нижче).
        Зчитування кнопки "О" для можливості підтвердження змін та зміни значення sets на false.
     4)
        Зчитування кнопки "Х" для можливості зміни значення змінної reset на true і скидання всіх параметрів (окрім розкладу) до стандартних.
        Виведення на дисплей повідомлення про відповідність поточних налаштувань щодо стандартних.
        Зчитування кнопки "О" для можливості підтвердження змін та зміни значення sets на false та відміни скидання параметрів.

    Зміна кожного параметру дати відбувається таким чином:
        При sets == true кнопками "<" та ">" змінюється значення змінної date, від чого залежить обраний параметр дати.
        Після вибору параметра дати, за допомогою кнопки "Х" змінна dset набуває значення true;
        При dset == true кнопками "<" та ">" змінюється значення конкретного обраного параметру.
        Кнопка "О" підтверджує зміни і надає змінній dset значення false.
  */
  DateTime now = rtc.now();
  int setpar = set.setpar;
  int button = readButton();
  int ringdur = set.ringdur;
  bool sets = set.sets;
  int date = set.date;
  bool dset = set.dset;
  String year = String(now.year());
  String month = String(now.month());
  String day = String(now.day());
  String hour = String(now.hour());
  String minute = String(now.minute());
  if(month.length() < 2){
      month = "0" + String(now.month());
    }
    else {
      month = String(now.month());
    }
    if(day.length() < 2){
      day = "0" + String(now.day());
    }
    else {
      String day = String(now.day());
    }
    if(hour.length() < 2){
      hour = "0" + String(now.hour());
    }
    else {
      String hour = String(now.hour());
    }
    if(minute.length() < 2){
      minute = "0" + String(now.minute());
    }
    else{
      String minute = String(now.minute());
    }
  if(current_menu == 3){              //Перевірка відкритого меню
    if(sets == false){                //Зміна параметру вимкнена
      switch (button){                //Перевірка натиснутої кнопки
        case 1:                       //Кнопка <
          if (setpar!=1){
            setpar -=1;
            set.setpar = setpar;
          } else {
            setpar = 4;
            set.setpar = setpar;
          }
          break;
        case 2:                       //Кнопка X
          if(setpar == 2){
            current_menu = 6;
          }else{                           //Зміна параметру
            sets = true;
          } 
          set.sets = sets;
          break;
        case 3:                       //Кнопка О
          current_menu = 2;           //Повернення до головного меню налаштування
          prefs.putInt("RingDur", set.ringdur);
          break;
        case 4:                       //Кнопка >
          if (setpar!=4){
            setpar +=1;
            set.setpar = setpar;
          } else {
            setpar = 1;
            set.setpar = setpar;
          }
          break;
      }
    } else {                          //Якщо зміна параметру ввімкнена
      switch (setpar){                //Вибраний параметр
        case 1:                       //Тривалість дзвінка
          switch (button){
            case 1:
              if(ringdur!=1){
                ringdur -=1;
              }
              set.ringdur = ringdur;
              break;
            case 2:
              //sets = false;
              set.sets = sets;
              break;
            case 3:
              sets = false;
              set.sets = sets;
              break;
            case 4:
              if(ringdur!=30){
                ringdur +=1;
              }
              set.ringdur = ringdur;
              break;
          }
          break;
        case 2:                          //Налаштування тривоги
          
          break;
        case 3:                           //Дата та час
          if (dset == false){
            switch (button){
              case 1:
                if(date!=1){
                  date-=1;
                } else {
                  date=5;
                }
                set.date = date;
                break;
              case 2:
                dset = true;
                set.dset = dset;
                break;
              case 3:
                Before();
                sets = false;
                set.sets = sets;
                break;
              case 4:
                if(date!=5){
                  date+=1;
                } else {
                  date=1;
                }
                set.date = date;
                break;
            }
          }
          break;
        case 4:
          switch (button){
            case 2:
              set.ringdur = 5;
              set.aldur = 30;
              set.alnum = 3;
              set.alring = 5;
              set.alpause = 1;
              set.alwait = 10;
              prefs.putInt("AlDur",set.aldur);
              prefs.putInt("AlNum",set.alnum);
              prefs.putInt("AlRing",set.alring);
              prefs.putInt("AlPaus1",set.alpause);
              prefs.putInt("AlPaus2",set.alwait);
              delay(1000);
              set.sets = false;
              break;
            case 3:
              set.sets = false;
              break;
          }
          break;
      }
      if(set.dset == true){         //Якщо вибір параметру дати ввімкнено
        switch (date){
          case 1:
            //Зміна року
            switch (button){
              case 1:
                rtc.adjust(DateTime(now.year() - 1,now.month(),now.day(),now.hour(),now.minute(),now.second()));
                break;
              case 2:
                //dset = false;
                set.dset = dset;
                break;
              case 3:
                dset = false;
                set.dset = dset;
                break;
              case 4:
                rtc.adjust(DateTime(now.year() + 1,now.month(),now.day(),now.hour(),now.minute(),now.second()));
                break;
            }
            break;
          case 2:
            //Зміна місяця
            switch (button){
              case 1:
                if(now.month() != 1){
                  rtc.adjust(DateTime(now.year(),now.month() - 1,now.day(),now.hour(),now.minute(),now.second()));
                } else {
                  rtc.adjust(DateTime(now.year(),12,now.day(),now.hour(),now.minute(),now.second()));
                }
                break;
              case 2:
                //dset = false;
                set.dset = dset;
                break;
              case 3:
                dset = false;
                set.dset = dset;
                break;
              case 4:
                if(now.month() != 12){
                  rtc.adjust(DateTime(now.year(),now.month() + 1,now.day(),now.hour(),now.minute(),now.second()));
                } else {
                  rtc.adjust(DateTime(now.year(),1,now.day(),now.hour(),now.minute(),now.second()));
                }
                break;
            }
            break;
          case 3:
            //Зміна дня
            switch (button){
              case 1:
                if(now.day() != 1){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day()-1,now.hour(),now.minute(),now.second()));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),dayInMonth(),now.hour(),now.minute(),now.second()));
                }
                break;
              case 2:
                //dset = false;
                set.dset = dset;
                break;
              case 3:
                dset = false;
                set.dset = dset;
                break;
              case 4:
                if(now.day() != dayInMonth()){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day()+1,now.hour(),now.minute(),now.second()));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),1,now.hour(),now.minute(),now.second()));
                }
                break;
            }
            break;
          case 4:
            //Зміна години
            switch (button){
              case 1:
                if(now.hour() != 0){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour()-1,now.minute(),0));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),23,now.minute(),0));
                }
                break;
              case 2:
                //dset = false;
                set.dset = dset;
                break;
              case 3:
                dset = false;
                set.dset = dset;
                break;
              case 4:
                if(now.hour() != 23){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour()+1,now.minute(),0));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),0,now.minute(),0));
                }
                break;
            }
            break;
          case 5:
            //Зміна хвилин
            switch (button){
              case 1:
                if(now.minute() != 0){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour(),now.minute()-1,0));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour(),59,0));
                }
                break;
              case 2:
                //dset = false;
                set.dset = dset;
                break;
              case 3:
                dset = false;
                set.dset = dset;
                break;
              case 4:
                if(now.minute() != 59){
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour(),now.minute()+1,0));
                } else {
                  rtc.adjust(DateTime(now.year(),now.month(),now.day(),now.hour(),0,0));
                }
                break;
            }
            break;
        }
      }
    }
      switch (setpar){
        case 1:
          out.UpText = "Ring duration";
          if(set.sets == true){
            out.DownText = "Set: " + String(ringdur) + " sec.";
          } else {
            out.DownText = String(ringdur);
          }
          break;
        case 2:
          out.UpText = "Alarm";
          out.DownText = "settings";
          break;
        case 3:
          out.UpText = "Time";
          if(set.sets == true){
            if(set.dset == false){
              switch (set.date){
                case 1:
                  out.UpText = "Year";
                  out.DownText = year + ">" + month + "." + day + " " + hour + ":" + minute;
                  break;
                case 2:
                  out.UpText = "Month";
                  out.DownText = year + "<" + month + ">" + day + " " + hour + ":" + minute;
                  break;
                case 3:
                  out.UpText = "Day";
                  out.DownText = year + "." + month + "<" + day + ">" + hour + ":" + minute;
                  break;
                case 4:
                  out.UpText = "Hour";
                  out.DownText = year + "." + month + "." + day + "<" + hour + ">" + minute;
                  break;
                case 5:
                  out.UpText = "Minute";
                  out.DownText = year + "." + month + "." + day + " " + hour + "<" + minute;
                  break;
              }
            } else {
              out.UpText = "Set time";
              switch (set.date){
                case 1:
                  out.UpText = "Set year";
                  out.DownText = year + ">" + month + "." + day + " " + hour + ":" + minute;
                  break;
                case 2:
                  out.UpText = "Set month";
                  out.DownText = year + "<" + month + ">" + day + " " + hour + ":" + minute;
                  break;
                case 3:
                  out.UpText = "Set day";
                  out.DownText = year + "." + month + "<" + day + ">" + hour + ":" + minute;
                  break;
                case 4:
                  out.UpText = "Set hour";
                  out.DownText = year + "." + month + "." + day + "<" + hour + ">" + minute;
                  break;
                case 5:
                  out.UpText = "Set minute";
                  out.DownText = year + "." + month + "." + day + " " + hour + "<" + minute;
                  break;
              }
            }
          } else {
            out.DownText = year + "." + month + "." + day + " " + hour + ":" + minute;
          }
          break;
        case 4:
          if (set.sets == false){
            if(set.ringdur == 5 && set.aldur == 30 && set.alnum == 3 && set.alring == 5 && set.alpause == 1 && set.alwait == 10){
              out.UpText = "Settings are";
              out.DownText = "default";
            } else {
              out.UpText = "Reset";
              out.DownText = "";
            }
          } else {
            if(set.ringdur == 5 && set.aldur == 30 && set.alnum == 3 && set.alring == 5 && set.alpause == 1 && set.alwait == 10){
              out.UpText = "Settings are";
              out.DownText = "already default";
            } else {
              out.UpText = "Do you want to";
              out.DownText = "reset settings?";
            }
          }
          break;
      }
  } else {
    lcd.clear();
  }
}
/*!
  \brief Точка входу до меню налаштування розкладів.
*/
void sheduleMenu(){
  /*!
    Вивід тексту на дисплей
    \code
    out.UpText = "Shedule";
    out.DownText = "Settings";
    \endcode
    Зчитування кнопок для переходу між меню
    \code
    int button = readButton();
    switch (button){
      case 1:
        current_menu = 2;
        break;
      case 2:
        current_menu = 5;
        shed.NringNum = 0;
        shed.SringNum = 0;
        break;
      case 4:
        current_menu = 1;
        break;
    }
    \endcode
  */
  int lastShed = shed.shedNum;
  out.UpText = "Shedule";
  out.DownText = "Settings";
  int button = readButton();
  switch (button){
    case 1:
      current_menu = 2;
      break;
    case 2:
      current_menu = 5;
      shed.NringNum = 0;
      shed.SringNum = 0;
      break;
    case 4:
      current_menu = 1;
      break;
  }
  if(lastShed != shed.shedNum){
    Before();
  }
}
/*!
  \brief Налаштування часу дзвінків для звичайного розкладу.
*/
void NormalShedule (){
  /*!
    Спочатку вмикається вибір часу першого дзвінка, який одночасно є точкою початку відліку для подальших обчислень.
    \code
    if(shed.NringNum == 0){
      out.UpText = "Start time:";
      out.DownText = "Set " + String(shed.startHour) + ":" + String(shed.startMinute);
      switch (button){ //Зчитування кнопок
        case 1: // Зменшення стартового часу на 5 хв.
          if(shed.startMinute !=0){
            shed.startMinute-=5;
          } else {
            if(shed.startHour !=0){
              shed.startMinute=55;
              shed.startHour-=1;
            } else {
              shed.startMinute=55;
              shed.startHour=23;
            }
          }
          break;
        case 2: // Запис стартового часу до постійної пам'яті і перехід до налаштування часу кожного наступного дзвінка
          shed.NringNum+=1;
          prefs.putInt("startHour", shed.startHour);
          prefs.putInt("startMinute", shed.startMinute);
          break;
        case 3:// Відміна всіх змін і повернення до Shedules
          shed.set = false;
          break;
        case 4:// Збільшення стартового часу на 5 хв.
          if(shed.startMinute !=55){
            shed.startMinute+=5;
          } else {
            if(shed.startHour !=23){
              shed.startMinute=0;
              shed.startHour+=1;
            } else {
              shed.startMinute=0;
              shed.startHour=0;
            }
          }
          break;
      }
    }
    \endcode
    Після налаштування часу стартового дзвінка відбувається вивід на дисплей тексту "Time to .. ring" у першому рядку, де на місці .. стоїть значення змінної NringNum, збільшеної на 1 (оскільки для користувача, на відміну від точки зору коду, 1 дзвінок є стартовим, тоді як з точки зору коду стартовий і перший дзвінки є різними предметами) і значення масивнної змінної Nring під номером NringNum з текстом "min." у нижньому рядку.
    \code
    out.UpText = "Time to " + String(shed.NringNum+1) + " ring";
    out.DownText = String(shed.Nring[shed.NringNum]) + " min.";
    \endcode
    Далі йде зчитування кнопок, де 1 і 4 стандатртно позначають зменшення/збільшення значення змінної на 5 хвилин.
    Кнопка 2 збільшує лічильник дзвінка на 1, при цьому записуючи попереднє значення до постійної пам'яті. Проте якщо це був останній можливий час то відбувається обчислення максимального дзвінка (для уникнення конфліктів та небажаних результатів) та повертається до Shedules.
    Кнопка 3 виконує ті самі дії що й кнопка 3 за умови запису усіх дзвінків.
    \code
    else {
        out.UpText = "Time to " + String(shed.NringNum+1) + " ring";
        out.DownText = String(shed.Nring[shed.NringNum]) + " min.";
        switch (button){
          case 1:
            if(shed.Nring[shed.NringNum] !=0){
              shed.Nring[shed.NringNum]-=5;
            }
            break;
          case 2:
            if (shed.NringNum < 11){
              switch (shed.NringNum){
                case 1:
                  prefs.putInt("rn1", shed.Nring[shed.NringNum]);
                  break;
                case 2:
                  prefs.putInt("rn2", shed.Nring[shed.NringNum]);
                  break;
                case 3:
                  prefs.putInt("rn3", shed.Nring[shed.NringNum]);
                  break;
                case 4:
                  prefs.putInt("rn4", shed.Nring[shed.NringNum]);
                  break;
                case 5:
                  prefs.putInt("rn5", shed.Nring[shed.NringNum]);
                  break;
                case 6:
                  prefs.putInt("rn6", shed.Nring[shed.NringNum]);
                  break;
                case 7:
                  prefs.putInt("rn7", shed.Nring[shed.NringNum]);
                  break;
                case 8:
                  prefs.putInt("rn8", shed.Nring[shed.NringNum]);
                  break;
                case 9:
                  prefs.putInt("rn9", shed.Nring[shed.NringNum]);
                  break;
                case 10:
                  prefs.putInt("rn10", shed.Nring[shed.NringNum]);
                  break;
                case 11:
                  prefs.putInt("rn11", shed.Nring[shed.NringNum]);
                  break;
              }
              shed.NringNum+=1;
            } else {
              shed.set = false;
              shed.maxN =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Nring[i] != 0){
                  shed.maxN +=1;
                }
              }
              shed.maxS =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Sring[i] != 0){
                  shed.maxS +=1;
                }
              }
            }
            break;
          case 3:
            shed.set = false;
            shed.maxN =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Nring[i] != 0){
                shed.maxN +=1;
              }
            }
            shed.maxS =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Sring[i] != 0){
                shed.maxS +=1;
              }
            }
            break;
          case 4:
            shed.Nring[shed.NringNum]+=5;
            break;
        }
      }
    \endcode
  */
  if(shed.NringNum == 0){
    out.UpText = "Start time:";
    out.DownText = "Set " + String(shed.startHour) + ":" + String(shed.startMinute);
    switch (button){ //Зчитування кнопок
      case 1: // Зменшення стартового часу на 5 хв.
        if(shed.startMinute !=0){
          shed.startMinute-=5;
        } else {
          if(shed.startHour !=0){
            shed.startMinute=55;
            shed.startHour-=1;
          } else {
            shed.startMinute=55;
            shed.startHour=23;
          }
        }
        break;
      case 2: // Запис стартового часу до постійної пам'яті і перехід до налаштування часу кожного наступного дзвінка
        shed.NringNum+=1;
        prefs.putInt("startHour", shed.startHour);
        prefs.putInt("startMinute", shed.startMinute);
        break;
      case 3:// Відміна всіх змін і повернення до Shedules
        shed.set = false;
        break;
      case 4:// Збільшення стартового часу на 5 хв.
        if(shed.startMinute !=55){
          shed.startMinute+=5;
        } else {
          if(shed.startHour !=23){
            shed.startMinute=0;
            shed.startHour+=1;
          } else {
            shed.startMinute=0;
            shed.startHour=0;
          }
        }
        break;
    }
  } else {
        out.UpText = "Time to " + String(shed.NringNum+1) + " ring";
        out.DownText = String(shed.Nring[shed.NringNum]) + " min.";
        switch (button){
          case 1:
            if(shed.Nring[shed.NringNum] !=0){
              shed.Nring[shed.NringNum]-=5;
            }
            break;
          case 2:
            if (shed.NringNum < 11){
              switch (shed.NringNum){
                case 1:
                  prefs.putInt("rn1", shed.Nring[shed.NringNum]);
                  break;
                case 2:
                  prefs.putInt("rn2", shed.Nring[shed.NringNum]);
                  break;
                case 3:
                  prefs.putInt("rn3", shed.Nring[shed.NringNum]);
                  break;
                case 4:
                  prefs.putInt("rn4", shed.Nring[shed.NringNum]);
                  break;
                case 5:
                  prefs.putInt("rn5", shed.Nring[shed.NringNum]);
                  break;
                case 6:
                  prefs.putInt("rn6", shed.Nring[shed.NringNum]);
                  break;
                case 7:
                  prefs.putInt("rn7", shed.Nring[shed.NringNum]);
                  break;
                case 8:
                  prefs.putInt("rn8", shed.Nring[shed.NringNum]);
                  break;
                case 9:
                  prefs.putInt("rn9", shed.Nring[shed.NringNum]);
                  break;
                case 10:
                  prefs.putInt("rn10", shed.Nring[shed.NringNum]);
                  break;
                case 11:
                  prefs.putInt("rn11", shed.Nring[shed.NringNum]);
                  break;
              }
              shed.NringNum+=1;
            } else {
              shed.set = false;
              shed.maxN =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Nring[i] != 0){
                  shed.maxN +=1;
                }
              }
              shed.maxS =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Sring[i] != 0){
                  shed.maxS +=1;
                }
              }
            }
            break;
          case 3:
            shed.set = false;
            shed.maxN =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Nring[i] != 0){
                shed.maxN +=1;
              }
            }
            shed.maxS =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Sring[i] != 0){
                shed.maxS +=1;
              }
            }
            break;
          case 4:
            shed.Nring[shed.NringNum]+=5;
            break;
        }
      }
}
/*!
  \brief Налаштування часу дзвінків для короткого розкладу.
*/
void ShortShedule (){
/*!
  Ця функція працює аналогічно до NormalShedule, за вийнятком різних оперованих змінних (Sring та SringNum).
*/
  if(shed.SringNum == 0){
        out.UpText = "Start time:";
        out.DownText = "Set " + String(shed.startHour) + ":" + String(shed.startMinute);
        switch (button){
          case 1:
            if(shed.startMinute !=0){
              shed.startMinute-=5;
            } else {
              if(shed.startHour !=0){
                shed.startMinute=55;
                shed.startHour-=1;
              } else {
                shed.startMinute=55;
                shed.startHour=23;
              }
            }
            break;
          case 2:
            shed.SringNum+=1;
            prefs.putInt("startHour", shed.startHour);
            prefs.putInt("startMinute", shed.startMinute);
            break;
          case 3:
            shed.set = false;
            break;
          case 4:
            if(shed.startMinute !=55){
              shed.startMinute+=5;
            } else {
              if(shed.startHour !=23){
                shed.startMinute=0;
                shed.startHour+=1;
              } else {
                shed.startMinute=0;
                shed.startHour=0;
              }
            }
            break;
        }
      } else {
        out.UpText = "Time to " + String(shed.SringNum+1) + " ring";
        out.DownText = String(shed.Sring[shed.SringNum]) + " min.";
        switch (button){
          case 1:
            if(shed.Sring[shed.SringNum] !=0){
              shed.Sring[shed.SringNum]-=5;
            }
            break;
          case 2:
            if (shed.SringNum < 11){
              switch (shed.SringNum){
                case 1:
                  prefs.putInt("rs1", shed.Sring[shed.SringNum]);
                  break;
                case 2:
                  prefs.putInt("rs2", shed.Sring[shed.SringNum]);
                  break;
                case 3:
                  prefs.putInt("rs3", shed.Sring[shed.SringNum]);
                  break;
                case 4:
                  prefs.putInt("rs4", shed.Sring[shed.SringNum]);
                  break;
                case 5:
                  prefs.putInt("rs5", shed.Sring[shed.SringNum]);
                  break;
                case 6:
                  prefs.putInt("rs6", shed.Sring[shed.SringNum]);
                  break;
                case 7:
                  prefs.putInt("rs7", shed.Sring[shed.SringNum]);
                  break;
                case 8:
                  prefs.putInt("rs8", shed.Sring[shed.SringNum]);
                  break;
                case 9:
                  prefs.putInt("rs9", shed.Sring[shed.SringNum]);
                  break;
                case 10:
                  prefs.putInt("rs10", shed.Sring[shed.SringNum]);
                  break;
                case 11:
                  prefs.putInt("rs11", shed.Sring[shed.SringNum]);
                  break;
              }
              shed.SringNum+=1;
            } else {
              shed.set = false;
              shed.maxN =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Nring[i] != 0){
                  shed.maxN +=1;
                }
              }
              shed.maxS =0;
              for(int i = 1; i <= 11;i++){
                if(shed.Sring[i] != 0){
                  shed.maxS +=1;
                }
              }
            }
            break;
          case 3:
            shed.set = false;
            shed.maxN =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Nring[i] != 0){
                shed.maxN +=1;
              }
            }
            shed.maxS =0;
            for(int i = 1; i <= 11;i++){
              if(shed.Sring[i] != 0){
                shed.maxS +=1;
              }
            }
            break;
          case 4:
            shed.Sring[shed.SringNum]+=5;
            break;
        }
      }
}
/*!
  \brief Меню вибору розкладу для налаштування.
*/
void Shedules(){
  /*!
    Синхронізація локальних змінних з класовими
    \code
    int shedNum = shed.shedNum;
    int button = readButton();
    \endcode
    Вивід на дисплей надпис "Set shedule" і текстову змінну shedType класу Shedule, що виводить назву поточного обраного розкладу відповідно до змінної shedNum того ж класу.
    \code
    out.UpText = "Set shedule:";
    shed.ShedText();
    out.DownText = String(shed.shedType);
    \endcode
    Зчитування кнопок для можливості зміни значення змінної shedNum, повернення до меню Розклад (sheduleMenu) або переходу до налаштування конкретного розкладу.
    \code
    switch (button){
      case 1:
        if(shedNum == 1){
          shedNum = 2;
        } else {
          shedNum = 1;
        }
        shed.shedNum = shedNum;// Номер розкладу для налаштування
        break;
      case 2:
        // Обнулювання усіх лічильників дзвінків і перехід до їх налаштування
        shed.NringNum = 0;
        shed.SringNum = 0;
        shed.set = true;
        break;
      case 3:
        current_menu = 4;// Повернення до sheduleMenu
        break;
      case 4:
        if(shedNum == 1){
          shedNum = 2;
        } else {
          shedNum = 1;
        }
        shed.shedNum = shedNum;
        break;
    }
    \endcode
    Коли ввімкнене налаштування конкретного розкладу (змінна set == true) в залежності від обраного типу розкладу відбувається перехід до NormalShedule або ShortShedule
    \code
    if (shed.shedNum == 1){
      NormalShedule();
    } else if (shed.shedNum == 2){
      ShortShedule();
    }
    \endcode
  */
  int shedNum = shed.shedNum;
  int button = readButton();
  if(shed.set == false){
    out.UpText = "Set shedule:";
    shed.ShedText();
    out.DownText = String(shed.shedType);
    switch (button){
      case 1:
        if(shedNum == 1){
          shedNum = 2;
        } else {
          shedNum = 1;
        }
        shed.shedNum = shedNum;// Номер розкладу для налаштування
        break;
      case 2:
        // Обнулювання усіх лічильників дзвінків і перехід до їх налаштування
        shed.NringNum = 0;
        shed.SringNum = 0;
        shed.set = true;
        break;
      case 3:
        current_menu = 4;// Повернення до sheduleMenu
        break;
      case 4:
        if(shedNum == 1){
          shedNum = 2;
        } else {
          shedNum = 1;
        }
        shed.shedNum = shedNum;
        break;
    }
  } else {
    if (shed.shedNum == 1){
      NormalShedule();
    } else if (shed.shedNum == 2){
      ShortShedule();
    }
  }
}
/*!
  \brief Меню налаштування параметрів алярми.
*/
void Alarm(){
  /*!
    Ця функція працює аналогічно з Settings, за вийнятком налаштовуваних змінних.
    Синхронізація локальних змінних з класовими
    \code
    int setal = set.setal;
    bool sets = set.sets;
    int button = readButton();
    \endcode
    На відміну від Settings, що використовує змінну set для вибору параметру і його налаштування, Alarm використовує власну змінну sets, проте тим самим способом.
    Вибір параметру:
    \code
    if(sets == false){
      switch (button){// Зчитування кнопок
        case 1: // Зміна параметру для налаштування подібно до setpar у Settings
          if(setal != 1){
            setal -=1;
          } else {
            setal = 5;
          }
          break;
        case 2:
          sets = true;
          break;
        case 3:
          sets = false;
          current_menu = 3;// Повернення до Settings
          break;
        case 4:
          if(setal != 5){
            setal +=1;
          } else {
            setal = 1;
          }
          break;
      }
      switch (setal){
        case 1:
          out.UpText = "Alarm duration";//Вивід назви параметру
          out.DownText = String(set.aldur) + " sec.";//Вивід його поточного значення
          break;
        case 2:
          out.UpText = "Number of rings";
          out.DownText = String(set.alnum);
          break;
        case 3:
          out.UpText = "Ring duration";
          out.DownText = String(set.alring) + " sec.";
          break;
        case 4:
          out.UpText = "Pause 1";
          out.DownText = String(set.alpause) + " sec.";
          break;
        case 5:
          out.UpText = "Pause 2";
          out.DownText = String(set.alwait) + " sec.";
          break;
      }
    }
    \endcode
    При sets == true Вивід назви і значення параметру, а також зчитування кнопок і їх функціонал прописані індивідуально для кожного параметру
    \code
    } else {
      switch (setal){
        case 1:
          out.UpText = "Alarm duration";
          out.DownText = "Set " + String(set.aldur) + " sec.";
          switch (button){
            case 1://Зменшення значення
              if(set.aldur != 5){
                set.aldur -=5;
              }
              break;
            case 2://Перехід далі відсутній
              set.aldur = set.aldur;
              break;
            case 3://Збереження значення у постійну пам'ять і повернення до вибору параметру
              set.aldur = set.aldur;
              prefs.putInt("AlDur",set.aldur);
              sets = false;
              break;
            case 4://Збільшення значення
              if(set.aldur != 300){
                set.aldur +=5;
              }
              break;
          }
          break;
        case 2:
          out.UpText = "Number of rings";
          out.DownText = "Set " + String(set.alnum);
          switch (button){
            case 1:
              if(set.alnum != 1){
                set.alnum -=1;
              }
              break;
            case 2:
              set.alnum = set.alnum;
              break;
            case 3:
              set.alnum = set.alnum;
              prefs.putInt("AlNum",set.alnum);
              sets = false;
              break;
            case 4:
              if(set.alnum != 10){
                set.alnum +=1;
              }
              break;
          }
          break;
        case 3:
          out.UpText = "Ring duration";
          out.DownText = "Set " + String(set.alring) + " sec.";
          switch (button){
            case 1:
              if(set.alring != 1){
                set.alring -=1;
              }
              break;
            case 2:
              set.alring = set.alring;
              break;
            case 3:
              set.alring = set.alring;
              prefs.putInt("AlRing",set.alring);
              sets = false;
              break;
            case 4:
              if(set.alring != 10){
                set.alring +=1;
              }
              break;
          }
          break;
        case 4:
          out.UpText = "Pause 1";
          out.DownText = "Set " + String(set.alpause) + " sec.";
          switch (button){
            case 1:
              if(set.alpause != 1){
                set.alpause -=1;
              }
              break;
            case 2:
              set.alpause = set.alpause;
              break;
            case 3:
              set.alpause = set.alpause;
              prefs.putInt("AlPaus1",set.alpause);
              sets = false;
              break;
            case 4:
              if(set.alpause != 30){
                set.alpause +=1;
              }
              break;
          }
          break;
        case 5:
          out.UpText = "Pause 2";
          out.DownText = "Set " + String(set.alwait) + " sec.";
          switch (button){
            case 1:
              if(set.alwait != 1){
                set.alwait -=1;
              }
              break;
            case 2:
              set.alwait = set.alwait;
              break;
            case 3:
              set.alwait = set.alwait;
              prefs.putInt("AlPaus2",set.alwait);
              sets = false;
              break;
            case 4:
              if(set.alwait != 30){
                set.alwait +=1;
              }
              break;
          }
          break;
      }
    }
    \endcode
  */
  int setal = set.setal;
  bool sets = set.sets;
  int button = readButton();
  if(sets == false){
    switch (button){// Зчитування кнопок
      case 1: // Зміна параметру для налаштування подібно до setpar у Settings
        if(setal != 1){
          setal -=1;
        } else {
          setal = 5;
        }
        break;
      case 2:
        sets = true;
        break;
      case 3:
        sets = false;
        current_menu = 3;// Повернення до Settings
        break;
      case 4:
        if(setal != 5){
          setal +=1;
        } else {
          setal = 1;
        }
        break;
    }
    switch (setal){
      case 1:
        out.UpText = "Alarm duration";//Вивід назви параметру
        out.DownText = String(set.aldur) + " sec.";//Вивід його поточного значення
        break;
      case 2:
        out.UpText = "Number of rings";
        out.DownText = String(set.alnum);
        break;
      case 3:
        out.UpText = "Ring duration";
        out.DownText = String(set.alring) + " sec.";
        break;
      case 4:
        out.UpText = "Pause 1";
        out.DownText = String(set.alpause) + " sec.";
        break;
      case 5:
        out.UpText = "Pause 2";
        out.DownText = String(set.alwait) + " sec.";
        break;
    }
  } else {
    switch (setal){
      case 1:
        out.UpText = "Alarm duration";
        out.DownText = "Set " + String(set.aldur) + " sec.";
        switch (button){
          case 1://Зменшення значення
            if(set.aldur != 5){
              set.aldur -=5;
            }
            break;
          case 2://Перехід далі відсутній
            set.aldur = set.aldur;
            break;
          case 3://Збереження значення у постійну пам'ять і повернення до вибору параметру
            set.aldur = set.aldur;
            prefs.putInt("AlDur",set.aldur);
            sets = false;
            break;
          case 4://Збільшення значення
            if(set.aldur != 300){
              set.aldur +=5;
            }
            break;
        }
        break;
      case 2:
        out.UpText = "Number of rings";
        out.DownText = "Set " + String(set.alnum);
        switch (button){
          case 1:
            if(set.alnum != 1){
              set.alnum -=1;
            }
            break;
          case 2:
            set.alnum = set.alnum;
            break;
          case 3:
            set.alnum = set.alnum;
            prefs.putInt("AlNum",set.alnum);
            sets = false;
            break;
          case 4:
            if(set.alnum != 10){
              set.alnum +=1;
            }
            break;
        }
        break;
      case 3:
        out.UpText = "Ring duration";
        out.DownText = "Set " + String(set.alring) + " sec.";
        switch (button){
          case 1:
            if(set.alring != 1){
              set.alring -=1;
            }
            break;
          case 2:
            set.alring = set.alring;
            break;
          case 3:
            set.alring = set.alring;
            prefs.putInt("AlRing",set.alring);
            sets = false;
            break;
          case 4:
            if(set.alring != 10){
              set.alring +=1;
            }
            break;
        }
        break;
      case 4:
        out.UpText = "Pause 1";
        out.DownText = "Set " + String(set.alpause) + " sec.";
        switch (button){
          case 1:
            if(set.alpause != 1){
              set.alpause -=1;
            }
            break;
          case 2:
            set.alpause = set.alpause;
            break;
          case 3:
            set.alpause = set.alpause;
            prefs.putInt("AlPaus1",set.alpause);
            sets = false;
            break;
          case 4:
            if(set.alpause != 30){
              set.alpause +=1;
            }
            break;
        }
        break;
      case 5:
        out.UpText = "Pause 2";
        out.DownText = "Set " + String(set.alwait) + " sec.";
        switch (button){
          case 1:
            if(set.alwait != 1){
              set.alwait -=1;
            }
            break;
          case 2:
            set.alwait = set.alwait;
            break;
          case 3:
            set.alwait = set.alwait;
            prefs.putInt("AlPaus2",set.alwait);
            sets = false;
            break;
          case 4:
            if(set.alwait != 30){
              set.alwait +=1;
            }
            break;
        }
        break;
    }
  }
  set.setal = setal;
  set.sets = sets;
}
/*!
  \brief Функція, що відповідає за правильне спрацювання реле
*/
void Ringing (){
  /*!
  Ця функція залежить від глобальної змінної last.
  Також вона спрацьовує лише при активному одному з головних меню.
  Це було зроблено для запобігання ввімкнення реле в непотрібний для цього час (під час налаштування або після закінчення робочого дня і до початку наступного).
  Додатково під час роботи реле на дисплеї в правому нижньому куті з'являється знак "!".
  Після вимкнення реле виконується перевірка чи це був останній дзвінок на це робочий день. Якщо так то змінна last набуває значення true, чим блокує спрацювання реле до наступного робочого дня. 
  Також в кінці відбувається корекція номеру останнього дзвінка для уникнення неточностей в обчисленнях.
  */
  DateTime now = rtc.now();
  if(last == false){
    if(current_menu == 1 || current_menu == 2 || current_menu == 4){
      digitalWrite(REL_PIN, LOW); // Ввімкнення реле
      out.UpText = String(now.year()) + "." + String(now.month()) + "." + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute());
      for (;out.UpText.length() < 16;){
        out.UpText = String(out.UpText) + " ";
      }
      out.DownText[15] = '!';
      for (;out.DownText.length() < 16;){
        out.DownText = String(out.DownText) + " ";
      }
      lcd.setCursor(0,1);
      lcd.print(out.DownText);
      delay(set.ringdur * 1000);
      out.DownText[15] = ' ';
      digitalWrite(REL_PIN, HIGH);
      if((shed.nextNum == shed.maxN && shed.shedNum == 1) || (shed.nextNum == shed.maxS && shed.shedNum == 2)){
        last = true;
      }
      if((shed.nextNum != 0 && shed.nextNum < shed.maxN && shed.shedNum == 1) || (shed.nextNum != 0 && shed.nextNum < shed.maxS && shed.shedNum == 2)){
        last = false;
      }
      ///
      if((shed.nextNum != shed.maxN && shed.shedNum == 1) || (shed.nextNum != shed.maxS && shed.shedNum == 2)){
        shed.nextNum += 1;
          if (shed.shedNum == 1){
              for (int i = 0; i < shed.Nring[shed.nextNum];i++){
                if(shed.nextM != 59){
                  shed.nextM += 1;
                } else {
                  if(shed.nextH != 23){
                    shed.nextM = 0;
                    shed.nextH+=1;
                  } else {
                    shed.nextM = 0;
                    shed.nextH = 0;
                  }
                }
              }
          } else if (shed.shedNum == 2){
              for (int i = 0; i < shed.Sring[shed.nextNum];i++){
                if(shed.nextM != 59){
                  shed.nextM += 1;
                } else {
                  if(shed.nextH != 23){
                    shed.nextM = 0;
                    shed.nextH+=1;
                  } else {
                    shed.nextM = 0;
                    shed.nextH = 0;
                  }
                }
              }
          }
      }
      ///
    }
  }
  shed.maxN =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Nring[i] != 0){
      shed.maxN +=1;
    }
  }
  shed.maxS =0;
  for(int i = 1; i <= 11;i++){
    if(shed.Sring[i] != 0){
      shed.maxS +=1;
    }
  }
  
}
/*!
  \brief Обчислення часу наступного дзвінка і умов його спрацювання
*/
void NextRing (){
  /*!
    Спочатку функція синхронізується з годинниковим модулем та вимикає реле в випадку закінчення робочого дня.
    \code
    DateTime now = rtc.now();
    if (shed.shedNum == 1){
      if(shed.nextNum > shed.maxN){
        shed.nextH = shed.startHour;
        shed.nextM = shed.startMinute;
        shed.nextNum = 0;
        last = true;
      }
    } else {
      if(shed.nextNum > shed.maxS){
        shed.nextH = shed.startHour;
        shed.nextM = shed.startMinute;
        shed.nextNum = 0;
        last = true;
      }
    }
    \endcode
    Далі йде порівняння даних з годинника з часом стартового або наступного дзвінка.
    Є три види порівнянь:
    1) Якщо поточний час менший за час стартового дзвінка:
    \code
    if(now.hour() < shed.startHour || (now.hour() <= shed.startHour && now.minute() < shed.startMinute)){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0; //Обнулення лічильника дзвінків
      last = false;
    }
    \endcode
    2) Якщо поточний час більший за час наступного дзвінка:
    \code
    if(now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
      // Відбувається обнулювання лічильника дзвінків
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
      if (shed.shedNum == 1){// Обчислення проводиться аналогічно для двох типів розкладів, але з використанням різних змінних
        for (;now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM);){
          for (int i = 0; i < shed.Nring[shed.nextNum];i++){ // Відбувається "промотування" лічильника часу на хвилину вперед доти, номер і час дзвінка не стане більшим за поточний
            if(shed.nextM != 59){
              shed.nextM += 1;
            } else {
              if(shed.nextH != 23){
                shed.nextM = 0;
                shed.nextH+=1;
              } else {
                shed.nextM = 0;
                shed.nextH = 0;
              }
            }
          }
          if (now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
            shed.nextNum+=1;
          }
        }
      } else if (shed.shedNum == 2){
        for (;now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM);){
          for (int i = 0; i < shed.Sring[shed.nextNum];i++){
            if(shed.nextM != 59){
              shed.nextM += 1;
            } else {
              if(shed.nextH != 23){
                shed.nextM = 0;
                shed.nextH+=1;
              } else {
                shed.nextM = 0;
                shed.nextH = 0;
              }
            }
          }
          if (now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
            shed.nextNum+=1;
          }
        }
      }
    \endcode
    3) Якщо поточний час дорівнює часу наступного дзвінка то відбувається лише виклик функції Ringing.
    Також в кінці відбувається перевірка на закінчення робочого дня (перевірка, чи не більше значення наступного дзвінка за максимальне):
    \code
    if(shed.shedNum == 1){
      if(shed.nextNum > shed.maxN){
        shed.nextH = shed.startHour;
        shed.nextM = shed.startMinute;
        shed.nextNum = 0;
      }
    }
    if(shed.shedNum == 2){
      if(shed.nextNum > shed.maxS){
        shed.nextH = shed.startHour;
        shed.nextM = shed.startMinute;
        shed.nextNum = 0;
      }
    }
    \endcode
  */
  DateTime now = rtc.now();
  if (shed.shedNum == 1){
    if(shed.nextNum > shed.maxN){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
      last = true;
    }
  } else {
    if(shed.nextNum > shed.maxS){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
      last = true;
    }
  }
  
    if(now.hour() < shed.startHour || (now.hour() <= shed.startHour && now.minute() < shed.startMinute)){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
      last = false;
    } else if (last == false){
      if(now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
        // Відбувається обнулювання лічильника дзвінків
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
      if (shed.shedNum == 1){// Обчислення проводиться аналогічно для двох типів розкладів, але з використанням різних змінних
        for (;now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM);){
          for (int i = 0; i < shed.Nring[shed.nextNum];i++){ // Відбувається "промотування" лічильника часу на хвилину вперед доти, номер і час дзвінка не стане більшим за поточний
            if(shed.nextM != 59){
              shed.nextM += 1;
            } else {
              if(shed.nextH != 23){
                shed.nextM = 0;
                shed.nextH+=1;
              } else {
                shed.nextM = 0;
                shed.nextH = 0;
              }
            }
          }
          if (now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
            shed.nextNum+=1;
          }
        }
      } else if (shed.shedNum == 2){
        for (;now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM);){
          for (int i = 0; i < shed.Sring[shed.nextNum];i++){
            if(shed.nextM != 59){
              shed.nextM += 1;
            } else {
              if(shed.nextH != 23){
                shed.nextM = 0;
                shed.nextH+=1;
              } else {
                shed.nextM = 0;
                shed.nextH = 0;
              }
            }
          }
          if (now.hour() > shed.nextH || (now.hour() >= shed.nextH && now.minute() > shed.nextM)){
            shed.nextNum+=1;
          }
        }
      }
  }
  else if(now.hour() == shed.nextH && now.minute() == shed.nextM){
      /*if((shed.nextNum != shed.maxN && shed.shedNum == 1) || (shed.nextNum != shed.maxS && shed.shedNum == 2)){
      shed.nextNum += 1;
        if (shed.shedNum == 1){
            for (int i = 0; i < shed.Nring[shed.nextNum];i++){
              if(shed.nextM != 59){
                shed.nextM += 1;
              } else {
                if(shed.nextH != 23){
                  shed.nextM = 0;
                  shed.nextH+=1;
                } else {
                  shed.nextM = 0;
                  shed.nextH = 0;
                }
              }
            }
        } else if (shed.shedNum == 2){
            for (int i = 0; i < shed.Sring[shed.nextNum];i++){
              if(shed.nextM != 59){
                shed.nextM += 1;
              } else {
                if(shed.nextH != 23){
                  shed.nextM = 0;
                  shed.nextH+=1;
                } else {
                  shed.nextM = 0;
                  shed.nextH = 0;
                }
              }
            }
        }
    }*/
    Ringing();
  }
    }
  if(shed.shedNum == 1){
    if(shed.nextNum > shed.maxN){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
    }
  }// else 
  if(shed.shedNum == 2){
    if(shed.nextNum > shed.maxS){
      shed.nextH = shed.startHour;
      shed.nextM = shed.startMinute;
      shed.nextNum = 0;
    }
  }
}

bool al = false;
/*!
  \brief Робота алярми
*/
void Alert(){
  /*!
    Ця функція виконуєтся при активуванні кнопки "Алярм" і деактивується лише при її деактивації і закінченні її дії.
    Перевірка чи закінчився загальний час алярми (параметр Alarm duration)
    \code
    if(millis() - timing < set.aldur*1000){
      al = true;
    } else {
      al = false;
    }
    \endcode
    Якщо час не закінчився, то відбувається такий самий вивід символів як при Ringing
    \code
      DateTime now = rtc.now();
      out.UpText = String(now.year()) + "." + String(now.month()) + "." + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute());
      lcd.setCursor(0,0);
      for (;out.UpText.length() <= 16;){
        out.UpText = String(out.UpText) + " ";
      }
      lcd.print(out.UpText);
      for (int i = 0;i <= 16;i++){
        out.DownText[i] = '!';
      }
      lcd.setCursor(0,1);
      lcd.print(out.DownText);
    \endcode
    Далі відбувається керування реле в заданому порядку
    \code
      for (int i = 0;i<=set.alnum;i++){ // Цикл відбувається кількість разів, задану в параметрі Number of Rings
        digitalWrite(REL_PIN, LOW); // Ввімкнення реле
        delay(set.alring*1000); // Час роботи реле, зазначений в параметрі Ring Duration
        digitalWrite(REL_PIN, HIGH);// Вимкнення реле
        delay(set.alpause*1000);// Коротка перерва між дзвінками (параметр Pause 1)
      }
      delay((set.alwait*1000) - (set.alpause*1000)); // Довга пауза між групами дзвінків (Pause 2)
      if(millis() - timing < set.aldur*1000){ // Додаткова перевірка на закінчення загального часу Алярми
        al = true;
      } else {
        al = false;
      }
    \endcode
  */
  if(millis() - timing < set.aldur*1000){
    al = true;
  } else {
    al = false;
  }
  if(al == true){
    for(;millis() - timing < set.aldur*1000;){
      Serial.println("Alarm!");
      DateTime now = rtc.now();
      out.UpText = String(now.year()) + "." + String(now.month()) + "." + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute());
      lcd.setCursor(0,0);
      for (;out.UpText.length() <= 16;){
        out.UpText = String(out.UpText) + " ";
      }
      lcd.print(out.UpText);
      for (int i = 0;i <= 16;i++){
        out.DownText[i] = '!';
      }
      lcd.setCursor(0,1);
      lcd.print(out.DownText);
      for (int i = 0;i<=set.alnum;i++){ // Цикл відбувається кількість разів, задану в параметрі Number of Rings
        digitalWrite(REL_PIN, LOW); // Ввімкнення реле
        Serial.println("AlRing");
        delay(set.alring*1000); // Час роботи реле, зазначений в параметрі Ring Duration
        digitalWrite(REL_PIN, HIGH);// Вимкнення реле
        Serial.println("AlPause");
        delay(set.alpause*1000);// Коротка перерва між дзвінками (параметр Pause 1)
      }
      Serial.println("AlWait");
      delay((set.alwait*1000) - (set.alpause*1000)); // Довга пауза між групами дзвінків (Pause 2)
      if(millis() - timing < set.aldur*1000){ // Додаткова перевірка на закінчення загального часу Алярми
        al = true;
      } else {
        al = false;
      }
    }
  }
}
/*!
  \brief "Стовбур" коду, виконується постійно. Відповідає за умови виконання всіх інших функцій.
*/
void loop() {
  /*!
    Спочатку йде перевірка справності годинника. Якщо є помилка, на дисплеї буде відображено текст про помилку і роботу пристрою буде призупинено.
    \code
    if (! rtc.begin()) {
      Serial.println("Couldn't find RTC");
      out.UpText = "Error!";
      out.DownText = "Timer isnt alright";
      while (1);
    }
    \endcode
    Якщо з годинником все гаразд, виконується вивід змінних out.UpText і out.DownText та їх форматування
    \code
    lcd.setCursor(0,0);
      for (;out.UpText.length() <= 16;){
        out.UpText = String(out.UpText) + " ";
      }
      lcd.print(out.UpText);
      for (;out.DownText.length() <= 16;){
        out.DownText = String(out.DownText) + " ";
      }
      lcd.setCursor(0,1);
      lcd.print(out.DownText);
    \endcode
    Далі виконується перевірка на алярму - Якщо вона активна то припиняється виконання усіх функцій окрім Alert.
    Якщо ні, то виконуються усі функції в залежності від змінної current_menu
    \code
    if(digitalRead(ALARM_PIN) == 1){
      Alert();
    } else {
      al = false;
      timing = millis();
      NextRing();
      switch (current_menu){
      case 1:
        mainMenu();
        break;
      case 2:
        settingsMenu();
        break;
      case 3:
        Settings();
        break;
      case 4:
        sheduleMenu();
        break;
      case 5:
        Shedules();
        break;
      case 6:
        Alarm();
        break;
      default:
        break;
      }
    }
    \endcode
    Також відбувається переривання на 10 мкс. для коректної роботи. 
  */
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    out.UpText = "Error!";
    out.DownText = "Timer isnt alright";
    while (1);
  } else {
    lcd.setCursor(0,0);
      for (;out.UpText.length() <= 16;){
        out.UpText = String(out.UpText) + " ";
      }
      lcd.print(out.UpText);
      for (;out.DownText.length() <= 16;){
        out.DownText = String(out.DownText) + " ";
      }
      lcd.setCursor(0,1);
      lcd.print(out.DownText);
    if(digitalRead(ALARM_PIN) == 1){
      Alert();
    } else {
      al = false;
      timing = millis();
      NextRing();
      switch (current_menu){
      case 1:
        mainMenu();
        break;
      case 2:
        settingsMenu();
        break;
      case 3:
        Settings();
        break;
      case 4:
        sheduleMenu();
        break;
      case 5:
        Shedules();
        break;
      case 6:
        Alarm();
        break;
      default:
        break;
      }
      
    }
  }
  delay(10);
}