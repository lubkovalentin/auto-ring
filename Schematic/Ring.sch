EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 800  4400 0    50   Input ~ 0
220
Text GLabel 800  4600 0    50   Input ~ 0
GND
$Comp
L Converter_ACDC:HLK-PM01 PS1
U 1 1 61E871D9
P 2700 4500
F 0 "PS1" H 2700 4825 50  0000 C CNN
F 1 "HLK-PM01" H 2700 4734 50  0000 C CNN
F 2 "Converter_ACDC:Converter_ACDC_HiLink_HLK-PMxx" H 2700 4200 50  0001 C CNN
F 3 "http://www.hlktech.net/product_detail.php?ProId=54" H 3100 4150 50  0001 C CNN
	1    2700 4500
	1    0    0    -1  
$EndComp
$Comp
L A-element:Something E1
U 1 1 61E88616
P 3750 6000
F 0 "E1" H 3750 5785 50  0000 C CNN
F 1 "Something" H 3750 5876 50  0000 C CNN
F 2 "" H 3750 6200 50  0001 C CNN
F 3 "" H 3750 6200 50  0001 C CNN
	1    3750 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 6000 1650 6000
Wire Wire Line
	1650 6000 1650 5650
$Comp
L Switch:SW_MEC_5G SW1
U 1 1 61E89BB5
P 1850 5650
F 0 "SW1" H 1850 5935 50  0000 C CNN
F 1 "SW_MEC_5G" H 1850 5844 50  0000 C CNN
F 2 "" H 1850 5850 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 1850 5850 50  0001 C CNN
	1    1850 5650
	1    0    0    -1  
$EndComp
$Comp
L Relay:DIPxx-1Cxx-51x K1
U 1 1 61E8B8C9
P 8150 5400
F 0 "K1" V 7583 5400 50  0000 C CNN
F 1 "DIPxx-1Cxx-51x" V 7674 5400 50  0000 C CNN
F 2 "Relay_THT:Relay_StandexMeder_DIP_LowProfile" H 8600 5350 50  0001 L CNN
F 3 "https://standexelectronics.com/wp-content/uploads/datasheet_reed_relay_DIP.pdf" H 8150 5400 50  0001 C CNN
	1    8150 5400
	0    1    1    0   
$EndComp
$Comp
L Device:Fuse F1
U 1 1 61E8F56E
P 1650 5200
F 0 "F1" H 1710 5246 50  0000 L CNN
F 1 "Fuse" H 1710 5155 50  0000 L CNN
F 2 "" V 1580 5200 50  0001 C CNN
F 3 "~" H 1650 5200 50  0001 C CNN
	1    1650 5200
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_MEC_5G SW2
U 1 1 61E906A5
P 3200 5650
F 0 "SW2" H 3200 5935 50  0000 C CNN
F 1 "SW_MEC_5G" H 3200 5844 50  0000 C CNN
F 2 "" H 3200 5850 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 3200 5850 50  0001 C CNN
	1    3200 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 5650 2150 5650
Wire Wire Line
	3100 4600 3450 4600
Wire Wire Line
	3450 4600 3450 4000
$Comp
L A-element:DataModule D1
U 1 1 61E9CDD3
P 8350 3800
F 0 "D1" V 8396 3622 50  0000 R CNN
F 1 "DataModule" V 8305 3622 50  0000 R CNN
F 2 "" H 8350 3800 50  0001 C CNN
F 3 "" H 8350 3800 50  0001 C CNN
	1    8350 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8250 3450 8250 2500
Wire Wire Line
	8250 2500 7850 2500
Wire Wire Line
	8350 3450 8350 2400
Wire Wire Line
	8450 2300 7750 2300
$Comp
L Device:R R1
U 1 1 61EB4F31
P 7900 1200
F 0 "R1" V 7693 1200 50  0000 C CNN
F 1 "R" V 7784 1200 50  0000 C CNN
F 2 "" V 7830 1200 50  0001 C CNN
F 3 "~" H 7900 1200 50  0001 C CNN
	1    7900 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	7750 1200 6850 1200
$Comp
L Switch:SW_MEC_5G SW3
U 1 1 61EB79DF
P 8450 1400
F 0 "SW3" V 8496 1352 50  0000 R CNN
F 1 "SW_MEC_5G" V 8405 1352 50  0000 R CNN
F 2 "" H 8450 1600 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 8450 1600 50  0001 C CNN
	1    8450 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8050 1200 8200 1200
Connection ~ 8200 1200
Wire Wire Line
	8200 1200 8450 1200
$Comp
L Device:R R2
U 1 1 61EBBB56
P 8800 1200
F 0 "R2" V 8593 1200 50  0000 C CNN
F 1 "R" V 8684 1200 50  0000 C CNN
F 2 "" V 8730 1200 50  0001 C CNN
F 3 "~" H 8800 1200 50  0001 C CNN
	1    8800 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	8650 1200 8450 1200
Connection ~ 8450 1200
$Comp
L Switch:SW_MEC_5G SW4
U 1 1 61EBCE75
P 9050 1400
F 0 "SW4" V 9096 1352 50  0000 R CNN
F 1 "SW_MEC_5G" V 9005 1352 50  0000 R CNN
F 2 "" H 9050 1600 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 9050 1600 50  0001 C CNN
	1    9050 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9050 1200 8950 1200
$Comp
L Device:R R3
U 1 1 61EC0C59
P 9400 1200
F 0 "R3" V 9193 1200 50  0000 C CNN
F 1 "R" V 9284 1200 50  0000 C CNN
F 2 "" V 9330 1200 50  0001 C CNN
F 3 "~" H 9400 1200 50  0001 C CNN
	1    9400 1200
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_MEC_5G SW5
U 1 1 61EC0C5F
P 9650 1400
F 0 "SW5" V 9696 1352 50  0000 R CNN
F 1 "SW_MEC_5G" V 9605 1352 50  0000 R CNN
F 2 "" H 9650 1600 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 9650 1600 50  0001 C CNN
	1    9650 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9650 1200 9550 1200
$Comp
L Device:R R4
U 1 1 61EC19D1
P 10050 1200
F 0 "R4" V 9843 1200 50  0000 C CNN
F 1 "R" V 9934 1200 50  0000 C CNN
F 2 "" V 9980 1200 50  0001 C CNN
F 3 "~" H 10050 1200 50  0001 C CNN
	1    10050 1200
	0    1    1    0   
$EndComp
$Comp
L Switch:SW_MEC_5G SW6
U 1 1 61EC19D7
P 10300 1400
F 0 "SW6" V 10346 1352 50  0000 R CNN
F 1 "SW_MEC_5G" V 10255 1352 50  0000 R CNN
F 2 "" H 10300 1600 50  0001 C CNN
F 3 "http://www.apem.com/int/index.php?controller=attachment&id_attachment=488" H 10300 1600 50  0001 C CNN
	1    10300 1400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10300 1200 10200 1200
Wire Wire Line
	9900 1200 9650 1200
Connection ~ 9650 1200
Wire Wire Line
	9250 1200 9050 1200
Connection ~ 9050 1200
Wire Wire Line
	10300 1600 9650 1600
Connection ~ 9050 1600
Wire Wire Line
	9050 1600 8450 1600
Connection ~ 9650 1600
Wire Wire Line
	9650 1600 9050 1600
Wire Wire Line
	10300 1600 10300 3400
Wire Wire Line
	10300 3400 6950 3400
Connection ~ 10300 1600
Wire Wire Line
	6950 3400 6950 4000
$Comp
L Device:LED D2
U 1 1 61EC9399
P 8450 2650
F 0 "D2" V 8489 2532 50  0000 R CNN
F 1 "LED" V 8398 2532 50  0000 R CNN
F 2 "" H 8450 2650 50  0001 C CNN
F 3 "~" H 8450 2650 50  0001 C CNN
	1    8450 2650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8450 2300 8450 2500
Wire Wire Line
	8450 2800 8450 3450
Wire Wire Line
	3400 5650 7850 5650
Wire Wire Line
	7850 5650 7850 5600
Wire Wire Line
	8450 5500 8550 5500
Wire Wire Line
	8550 5500 8550 6000
Wire Wire Line
	8550 6000 4000 6000
Wire Wire Line
	3450 4000 6950 4000
$Comp
L A-element:ESP32 ESP1
U 1 1 61EE7DFB
P 5850 1850
F 0 "ESP1" H 5850 2625 50  0000 C CNN
F 1 "ESP32" H 5850 2534 50  0000 C CNN
F 2 "" H 5850 2450 50  0001 C CNN
F 3 "" H 5850 2450 50  0001 C CNN
	1    5850 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4400 3100 2700
Wire Wire Line
	6850 1200 6850 1600
Wire Wire Line
	6850 1600 6300 1600
Wire Wire Line
	8200 1750 6900 1750
Wire Wire Line
	6900 1750 6900 3200
Wire Wire Line
	6900 3200 6300 3200
Wire Wire Line
	8200 1200 8200 1750
Wire Wire Line
	7850 2500 7850 3350
Wire Wire Line
	7850 3350 5300 3350
Wire Wire Line
	5300 3350 5300 2700
Connection ~ 5300 2700
Wire Wire Line
	5300 2700 5400 2700
$Comp
L A-element:i2c I2C1
U 1 1 61EF59DD
P 1650 1150
F 0 "I2C1" H 1878 1051 50  0000 L CNN
F 1 "i2c" H 1878 960 50  0000 L CNN
F 2 "" H 1650 1300 50  0001 C CNN
F 3 "" H 1650 1300 50  0001 C CNN
	1    1650 1150
	1    0    0    -1  
$EndComp
$Comp
L A-element:i2c I2C2
U 1 1 61EF61F8
P 2600 1150
F 0 "I2C2" H 2828 1051 50  0000 L CNN
F 1 "i2c" H 2828 960 50  0000 L CNN
F 2 "" H 2600 1300 50  0001 C CNN
F 3 "" H 2600 1300 50  0001 C CNN
	1    2600 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 2700 2450 2700
Connection ~ 3100 2700
Wire Wire Line
	3100 2700 5300 2700
Connection ~ 2450 2700
Wire Wire Line
	2450 2700 3100 2700
Wire Wire Line
	5400 2800 2550 2800
Wire Wire Line
	1600 2800 1600 1250
Wire Wire Line
	1500 1250 1500 2700
Wire Wire Line
	2450 1250 2450 2700
Wire Wire Line
	2550 1250 2550 2800
Connection ~ 2550 2800
Wire Wire Line
	2550 2800 1600 2800
Wire Wire Line
	1700 2300 2650 2300
Wire Wire Line
	4850 2300 4850 750 
Wire Wire Line
	4850 750  6550 750 
Wire Wire Line
	6550 750  6550 1800
Wire Wire Line
	6550 1800 6300 1800
Connection ~ 2650 2300
Wire Wire Line
	2650 2300 4850 2300
Wire Wire Line
	2650 1250 2650 2300
Wire Wire Line
	1700 1250 1700 2300
Wire Wire Line
	1800 1250 1800 2150
Wire Wire Line
	1800 2150 2750 2150
Wire Wire Line
	4750 2150 4750 850 
Wire Wire Line
	4750 850  6450 850 
Wire Wire Line
	6450 850  6450 1700
Wire Wire Line
	6450 1700 6300 1700
Connection ~ 2750 2150
Wire Wire Line
	2750 2150 4750 2150
Wire Wire Line
	2750 1250 2750 2150
$Comp
L Device:Fuse F2
U 1 1 61E9D3EC
P 2000 4400
F 0 "F2" V 1803 4400 50  0000 C CNN
F 1 "Fuse" V 1894 4400 50  0000 C CNN
F 2 "" V 1930 4400 50  0001 C CNN
F 3 "~" H 2000 4400 50  0001 C CNN
	1    2000 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	2300 4400 2150 4400
Wire Wire Line
	8450 4150 8450 5200
Wire Wire Line
	8250 4150 7850 4150
Wire Wire Line
	7850 4150 7850 5200
Wire Wire Line
	1650 5350 1650 5650
Connection ~ 1650 5650
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J1
U 1 1 61ED8003
P 1250 4450
F 0 "J1" H 1300 4767 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 1300 4676 50  0000 C CNN
F 2 "" H 1250 4450 50  0001 C CNN
F 3 "~" H 1250 4450 50  0001 C CNN
	1    1250 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 4400 1800 4400
Wire Wire Line
	1800 4400 1800 4350
Wire Wire Line
	1800 4350 1550 4350
Wire Wire Line
	800  4350 1050 4350
Wire Wire Line
	800  4450 1050 4450
Wire Wire Line
	800  4350 800  4450
Wire Wire Line
	1050 4550 800  4550
Wire Wire Line
	1050 4650 800  4650
Wire Wire Line
	800  4550 800  4650
Wire Wire Line
	1550 4450 1650 4450
Wire Wire Line
	1650 4450 1650 5050
Wire Wire Line
	1550 4550 1850 4550
Wire Wire Line
	1850 4550 1850 4600
Wire Wire Line
	1850 4600 2300 4600
Wire Wire Line
	1550 4650 2150 4650
Wire Wire Line
	2150 4650 2150 5650
Connection ~ 2150 5650
Wire Wire Line
	2150 5650 3000 5650
NoConn ~ 5400 2600
NoConn ~ 5400 2500
NoConn ~ 5400 2400
NoConn ~ 5400 2300
NoConn ~ 5400 2200
NoConn ~ 5400 2100
NoConn ~ 5400 2000
NoConn ~ 5400 1900
NoConn ~ 5400 1800
NoConn ~ 5400 1700
NoConn ~ 5400 1600
NoConn ~ 5400 1500
NoConn ~ 5400 1400
NoConn ~ 5400 1300
NoConn ~ 6300 1300
NoConn ~ 6300 1400
NoConn ~ 6300 1500
NoConn ~ 6300 1900
NoConn ~ 6300 2100
NoConn ~ 6300 2200
NoConn ~ 6300 2400
NoConn ~ 6300 2600
NoConn ~ 6300 2700
NoConn ~ 6300 2800
NoConn ~ 6300 2900
NoConn ~ 6300 3100
$Comp
L Transistor_BJT:BC547 Q1
U 1 1 61EF17B0
P 7200 2300
F 0 "Q1" H 7391 2346 50  0000 L CNN
F 1 "BC547" H 7391 2255 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7400 2225 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/BC550-D.pdf" H 7200 2300 50  0001 L CNN
	1    7200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2300 7750 2100
Wire Wire Line
	7750 2100 7300 2100
Wire Wire Line
	7000 2300 6300 2300
Wire Wire Line
	7300 2500 7300 3000
Wire Wire Line
	7300 3000 6300 3000
Wire Wire Line
	7550 2400 7550 3000
Wire Wire Line
	7550 3000 7300 3000
Wire Wire Line
	7550 2400 8350 2400
Connection ~ 7300 3000
$EndSCHEMATC
